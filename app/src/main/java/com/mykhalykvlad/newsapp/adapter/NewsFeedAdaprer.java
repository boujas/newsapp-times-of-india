package com.mykhalykvlad.newsapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mykhalykvlad.newsapp.FullNewsActivity;
import com.mykhalykvlad.newsapp.R;
import com.mykhalykvlad.newsapp.models.Image;
import com.mykhalykvlad.newsapp.models.News;
import com.mykhalykvlad.newsapp.databinding.NewsItemBinding;
import com.mykhalykvlad.newsapp.utils.NewsClickHandler;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by boujas on 24.04.2016.
 */
public class NewsFeedAdaprer extends RecyclerView.Adapter<NewsFeedAdaprer.ViewHolder> {

    private List<News> mDataset;
    private Context    mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected NewsItemBinding binding;
        protected CardView cv;

        public ViewHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
        }
    }

    public NewsFeedAdaprer(List<News> myDataset, Context context) {
        this.mContext = context;
        this.mDataset = myDataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        NewsItemBinding binding = NewsItemBinding.inflate(inflater, viewGroup, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder customViewHolder, int i) {
        final News news = mDataset.get(i);
        Image image = news.getImage();
        customViewHolder.binding.setNews(news);
        customViewHolder.binding.setImage(image);
        customViewHolder.binding.setClick(new NewsClickHandler() {
            @Override
            public void onOpenFullNewsClicked(View view) {
                Intent intent = new Intent(mContext, FullNewsActivity.class);
                intent.putExtra(mContext.getString(R.string.extras_param_news), news);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView imageView, String v) {
        Picasso.with(imageView.getContext()).load(v).into(imageView);
    }
}
