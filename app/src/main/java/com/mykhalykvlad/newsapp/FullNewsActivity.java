package com.mykhalykvlad.newsapp;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.mykhalykvlad.newsapp.fragments.FullNewsFragment;
import com.mykhalykvlad.newsapp.models.News;

public class FullNewsActivity extends AppCompatActivity {

    private FullNewsFragment mFullNewsFragment;
    private News mNews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_news);

        mNews = (News) getIntent().getExtras().get(getString(R.string.extras_param_news));
        if (mNews !=null) setupActionBar(mNews.getHeadLine());

        if (findViewById(R.id.fragment_full_news_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            mFullNewsFragment = new FullNewsFragment();
            mFullNewsFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_full_news_container, mFullNewsFragment).commit();
        }
    }
    private  void setupActionBar(String title){
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null) {
            actionBar.setTitle(title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
    @Override
    public void onBackPressed() {
        WebView webView = mFullNewsFragment.mWebView;

        if (webView.canGoBack()){
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}