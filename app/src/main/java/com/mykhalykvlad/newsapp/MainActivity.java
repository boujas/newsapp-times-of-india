package com.mykhalykvlad.newsapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mykhalykvlad.newsapp.fragments.NewsFeedFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }

            NewsFeedFragment newsFeed = new NewsFeedFragment();
            newsFeed.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, newsFeed).commit();
        }
    }
}
