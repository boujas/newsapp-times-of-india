package com.mykhalykvlad.newsapp.fragments;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mykhalykvlad.newsapp.R;
import com.mykhalykvlad.newsapp.adapter.NewsFeedAdaprer;
import com.mykhalykvlad.newsapp.api.NewsApi;
import com.mykhalykvlad.newsapp.databinding.NewsItemBinding;
import com.mykhalykvlad.newsapp.models.FeedItem;
import com.mykhalykvlad.newsapp.models.News;
import com.mykhalykvlad.newsapp.databinding.FragmentNewsFeedBinding;
import com.mykhalykvlad.newsapp.utils.ConnectionManager;



import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


public class NewsFeedFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ObservableArrayList<News> newsList;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private ConnectionManager connectionManager;
    protected FragmentNewsFeedBinding binding;

    public NewsFeedFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_feed, container, false);
        View view = binding.getRoot();
        init(view);
        getReport();
        return view;
    }

    private void init(View v) {
        connectionManager = new ConnectionManager(
                ConnectionManager.ConnectionStatusEnum.STATUS_CONNECTING, getActivity());
        newsList = new ObservableArrayList<>();
        binding.setNewsList(newsList);
        binding.setConnectionManager(connectionManager);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new NewsFeedAdaprer(newsList, getActivity());
        mRecyclerView.setAdapter(mAdapter);

        mySwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getReport();
                    }
                }
        );
    }

    private boolean isInternetConnectionAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void getReport() {

        if (!isInternetConnectionAvailable()) {
            connectionManager.setCurrentConnectionStatus(ConnectionManager.ConnectionStatusEnum.STATUS_NO_INTERNET);
            return;
        }
        connectionManager.setCurrentConnectionStatus(ConnectionManager.ConnectionStatusEnum.STATUS_CONNECTING);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NewsApi service = retrofit.create(NewsApi.class);
        Call<FeedItem> call = service.getNewsFeed();

        call.enqueue(new Callback<FeedItem>() {
            @Override
            public void onResponse(Response<FeedItem> response, Retrofit retrofit) {
                try {
                    mySwipeRefreshLayout.setRefreshing(false);
                    newsList.clear();
                    newsList.addAll(response.body().getNews());
                    mAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    mySwipeRefreshLayout.setRefreshing(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                mySwipeRefreshLayout.setRefreshing(false);
                connectionManager.setCurrentConnectionStatus(ConnectionManager.ConnectionStatusEnum.STATUS_ERROR);
            }
        });
    }

}
