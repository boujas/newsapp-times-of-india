package com.mykhalykvlad.newsapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mykhalykvlad.newsapp.R;
import com.mykhalykvlad.newsapp.models.News;

public class FullNewsFragment extends Fragment {

    public WebView mWebView;
    private News mNews;
    private String mFullNewsUrl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_full_news, container, false);
        mWebView = (WebView) v.findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new MyWebViewClient());

        mNews = (News) getArguments().get(getString(R.string.extras_param_news));
        if (mNews != null) mFullNewsUrl = mNews.getWebURL();

        mWebView.loadUrl(mFullNewsUrl);
        return v;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            mFullNewsUrl = url;
            view.loadUrl(url);
            return true;
        }
    }


}
