package com.mykhalykvlad.newsapp.api;

import com.mykhalykvlad.newsapp.models.FeedItem;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by boujas on 24.04.2016.
 */
public interface NewsApi {
    @GET("/feeds/newsdefaultfeeds.cms?feedtype=sjson")
    Call<FeedItem> getNewsFeed();
}
