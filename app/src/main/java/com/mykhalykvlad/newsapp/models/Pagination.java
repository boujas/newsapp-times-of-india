package com.mykhalykvlad.newsapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Pagination {

    @SerializedName("TotalPages")
    @Expose
    private String totalPages;
    @SerializedName("PageNo")
    @Expose
    private String pageNo;
    @SerializedName("PerPage")
    @Expose
    private String perPage;
    @SerializedName("WebURL")
    @Expose
    private String webURL;

    /**
     * 
     * @return
     *     The ToalPages
     */
    public String getTotalPages() {
        return totalPages;
    }

    /**
     * 
     * @param totalPages
     *     The TotalPages
     */
    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * 
     * @return
     *     The PageNo
     */
    public String getPageNo() {
        return pageNo;
    }

    /**
     * 
     * @param pageNo
     *     The PageNo
     */
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * 
     * @return
     *     The PerPage
     */
    public String getPerPage() {
        return perPage;
    }

    /**
     * 
     * @param perPage
     *     The PerPage
     */
    public void setPerPage(String perPage) {
        this.perPage = perPage;
    }

    /**
     * 
     * @return
     *     The WebURL
     */
    public String getWebURL() {
        return webURL;
    }

    /**
     * 
     * @param webURL
     *     The WebURL
     */
    public void setWebURL(String webURL) {
        this.webURL = webURL;
    }

}
