package com.mykhalykvlad.newsapp.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Image {

    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("Thumb")
    @Expose
    private String thumb;
    @SerializedName("PhotoCaption")
    @Expose
    private String photoCaption;

    /**
     * 
     * @return
     *     The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 
     * @param photo
     *     The Photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * 
     * @return
     *     The Thumb
     */
    public String getThumb() {
        return thumb;
    }

    /**
     * 
     * @param phumb
     *     The Thumb
     */
    public void setThumb(String phumb) {
        this.thumb = thumb;
    }

    /**
     * 
     * @return
     *     The PhotoCaption
     */
    public String getPhotoCaption() {
        return photoCaption;
    }

    /**
     * 
     * @param photoCaption
     *     The PhotoCaption
     */
    public void setPhotoCaption(String photoCaption) {
        this.photoCaption = photoCaption;
    }

}
