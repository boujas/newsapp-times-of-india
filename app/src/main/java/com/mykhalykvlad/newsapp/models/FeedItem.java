package com.mykhalykvlad.newsapp.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class FeedItem {

    @SerializedName("Pagination")
    @Expose
    private Pagination pagination;
    @SerializedName("NewsItem")
    @Expose
    private List<News> news = new ArrayList<News>();

    /**
     * 
     * @return
     *     The Pagination
     */
    public Pagination getPagination() {
        return pagination;
    }

    /**
     * 
     * @param pagination
     *     The Pagination
     */
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    /**
     * 
     * @return
     *     The News
     */
    public List<News> getNews() {
        return news;
    }

    /**
     * 
     * @param News
     *     The News
     */
    public void setNews(List<News> News) {
        this.news = News;
    }

}
