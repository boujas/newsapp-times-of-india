package com.mykhalykvlad.newsapp.utils;

import android.content.Context;

import com.mykhalykvlad.newsapp.R;

/**
 * Created by boujas on 5/12/16.
 */
public class ConnectionManager {

    public enum ConnectionStatusEnum {
        STATUS_CONNECTING,
        STATUS_NO_INTERNET,
        STATUS_ERROR
    }

    public String message;
    private Context context;
    public ConnectionStatusEnum currentConnectionStatus;


    public ConnectionManager() {
        currentConnectionStatus = ConnectionStatusEnum.STATUS_CONNECTING;
        setupStatusValues();
    }
    public ConnectionManager(ConnectionStatusEnum connectionStatus, Context context) {
        currentConnectionStatus = connectionStatus;
        this.context = context;
        setupStatusValues();
    }
    private void setupStatusValues(){
        switch (currentConnectionStatus) {
            case STATUS_CONNECTING:
                message = context.getString(R.string.status_connecting);
                break;
            case STATUS_NO_INTERNET:
                message = context.getString(R.string.status_no_internet);
                break;
            case STATUS_ERROR:
                message = context.getString(R.string.status_no_internet);
                break;
        }
    }

    public ConnectionStatusEnum getCurrentConnectionStatus() {
        return currentConnectionStatus;
    }

    public void setCurrentConnectionStatus(ConnectionStatusEnum currentConnectionStatus) {
        this.currentConnectionStatus = currentConnectionStatus;
        this.setupStatusValues();
    }

    public String getStatus() {
        return "ConnectionStatus{" +
                ", message='" + message + '\'' +
                ", currentConnectionStatus=" + currentConnectionStatus +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
