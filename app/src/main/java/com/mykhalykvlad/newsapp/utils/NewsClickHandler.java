package com.mykhalykvlad.newsapp.utils;

import android.view.View;

/**
 * Created by boujas on 12.05.2016.
 */
public interface NewsClickHandler {

    void onOpenFullNewsClicked(View view);

}
